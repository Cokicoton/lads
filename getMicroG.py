import requests
import sys
import htmlmin

silent = False
checkPrice = False
if len(sys.argv) > 1:
    if sys.argv[1] == 'silent':
        silent = True

url_microg = 'https://download.lineage.microg.org/'

def getMicroGVersion(data):
    versions = data.split('.zip</span>')
    del versions[-1]
    for version in versions:
        temp = version.split('<span class=name>lineage-')
        last = temp[len(version.split('<span class=name>lineage-'))-1]
        version = last.split('-')[0]
        date = last.split('-')[1]
    return version + '_' + date[6:8] + '/' + date[4:6] + '/' + date[0:4]

file = open('resultat.csv', "r")
liste_devices = file.read().split('\n')
file2 = open('resultat_microg.csv', "w")
entete = True
for device in liste_devices:
    if not entete:
        if len(device.split(';')) > 1:
            model = device.split(';')[6]
            if not silent:
                print(model)
            microG_url = url_microg + model + '/'
            # Récupération des données microG
            microG_data =htmlmin.minify(requests.get(microG_url, verify=False).text)
            if(len(microG_data.split('<tbody>')) > 1 and len(microG_data.split('<span class=name>lineage-')) > 1):
                microG_version_date = getMicroGVersion(microG_data.split('<tbody>')[1].split('</tbody>')[0])
                microG_version = microG_version_date.split('_')[0]
                microG_date = microG_version_date.split('_')[1]
                file2.write(device + ';' + microG_url + ';' + microG_version + ';' + microG_date)
            else:
                file2.write(device + ';;;')
    else:
        file2.write(device + ';URL microG;Version microG;Date version microG')
        entete = False
    file2.write('\n')
file.close()
file2.close()
