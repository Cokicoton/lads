# LADS (LineageOs available devices scraper)

Python script to retrieve all devices eligible for LineageOs.

By scraping the official page of LineageOs https://download.lineageos.org

In the form of a CSV file including different information about each device (brand, model, date of release, supported LineageOs version, RAM...)

An HTML export is also included but does not allow for the moment neither sorting nor searching

Another Python script adds microG information to the first script's data in another file.

## Getting started

Just run the get.py script. This one will generate the file resultat.csv and the web page under html/index.html

Run the getMicroG.py script to generate the file resultat_microg.csv (only if resultat.csv is generated).

## Options
By default the list of models is displayed in the terminal.

It is possible to display nothing by adding "silent" as first argument.

It is also possible to get the prices of the devices on Amazon.

By adding "price" as the first or second argument. Columns will be added to the right.

For getMicrog.py, it is possible to display nothing by adding 'silent' as the first argument too.

## Example of result
### To CSV
| Marque   | Modèle | Version | Année | Mois | Ram | Identifiant | URL device | URL build |
|----------|--------|---------|-------|------|-----|-------------|------------|-----------|
| ASUS     | ASUS ZenFone 8  | 19.1 | 2021 | 5 | 6/8/12/16 GB LPDDR5 | sake | https://wiki.lineageos.org/devices/sake/ | https://download.lineageos.org/sake |
| ASUS     | ASUS Zenfone Max Pro M1  | 19.1 | 2018 | 5 | 3/4/6 GB | X00TD | https://wiki.lineageos.org/devices/X00TD/ | https://download.lineageos.org/X00TD |
| ASUS     | ASUS Zenfone 5Z (ZS620KL)  | 20.0 | 2018 | 6 | 4/6/8 GB LPDDR4X | Z01R | https://wiki.lineageos.org/devices/Z01R/ | https://download.lineageos.org/Z01R |
| Dynalink | Dynalink TV Box (4K)  | 19.1 | 2021 | 6 | 2 GB | wade | https://wiki.lineageos.org/devices/wade/ | https://download.lineageos.org/wade |
|...|...|...|...|...|...|...|...|...|

### To HTML
![HTML example](readme/HTML_example.png)
