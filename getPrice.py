import requests
import os
import platform
import os
import sys

validSalers = ['Amazon Marketplace', 'Rakuten', 'Darty Marketplace', 'La Redoute Marketplace', 'Fnac.com Marketplace', 'Cdiscount Marketplace', 'Amazon']

def checkModel(a, b):
    temp = a.split(' ')
    cpt=0
    for part in temp:
        if part.upper() in b.upper():
            cpt = cpt+1
    if((len(temp)==cpt) or (len(temp)>3 and cpt==len(temp)-1)):
        return True
    else:
        return False


def getPrice(modele, getModel=False):
    search = modele.replace(' ', '%20')
    r = requests.get('https://www.lesnumeriques.com/recherche?q=' + search)
    open('price.txt', 'wb').write(r.content)
    file = open('price.txt', "r", encoding="utf8")
    content_price = file.read()
    file.close()
    blocksModels = content_price.split('data-track-action="')
    i=0
    arrayBlock = []
    for block in blocksModels:
        value=0
        if(len(content_price.split('data-track-action="')) > 1):
            saler = block.split('"')[0]
            temp = block
            if(len(temp.split('data-track-label="')) > 1):
                temp = temp.split('data-track-label="')[1]
                modeleAmazon = temp.split('"')[0]
                if saler in validSalers:
                    if modele.upper().strip() == modeleAmazon.upper().strip():
                        value = 5
                    elif checkModel(modele, modeleAmazon) and checkModel(modeleAmazon, modele):
                        value = 3
                    elif checkModel(modele, modeleAmazon):
                        value = 0
                    elif checkModel(modeleAmazon, modele):
                        value = 0
                    else:
                        value = 0
                else:
                    value = 0
        i=i+1
        arrayBlock.append(value)
    maxValue = max(arrayBlock)
    if maxValue > 0:
        i=0
        for a in arrayBlock:
            if a == maxValue:
                break
            i=i+1
    if(len(content_price.split('data-track-action="')) > 1 and maxValue > 0):
        temp = blocksModels[i]
        if(len(temp.split('data-track-label="')) > 1):
            temp = temp.split('data-track-label="')[1]
            modeleAmazon = temp.split('"')[0]
            if(len(temp.split('data-track-price="')) > 1):
                temp = temp.split('data-track-price="')[1]
                prixAmazon = temp.split('"')[0]
            else:
                modeleAmazon = 'NC'
                prixAmazon = 'NC'
        else:
            modeleAmazon = 'NC'
            prixAmazon = 'NC'
    else:
        modeleAmazon = 'NC'
        prixAmazon = 'NC'
    if(getModel):
        return [modeleAmazon, prixAmazon]
    else:
        return prixAmazon
