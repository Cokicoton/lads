import requests
import os
import platform
import sys
import getPrice
import json
import htmlmin

silent = False
checkPrice = False
if len(sys.argv) > 1:
    if sys.argv[1] == 'silent':
        silent = True
    elif sys.argv[1] == 'price':
        checkPrice = True
    elif len(sys.argv) > 2 and sys.argv[2] == 'price':
        checkPrice = True

apiUrl = 'https://download.lineageos.org/api/v2/'

limit = 0
months = {'January': '01', 'February': '02', 'March': '03', 'April': '04', 'May': '05', 'June': '06', 'July': '07', 'August': '08', 'September': '09', 'October': '10', 'November': '11', 'December': '12',}

if os.path.exists('resultat.csv'):
    os.remove('resultat.csv')
if os.path.exists('home.txt'):
    os.remove('home.txt')
if os.path.exists('devices_links_lineageos.txt'):
    os.remove('devices_links_lineageos.txt')
dir = 'temp'
for f in os.listdir(dir):
    os.remove(os.path.join(dir, f))

# Récupération de la liste des devices
r_home = requests.get(apiUrl + 'oems', verify=False)
listDevices = json.loads(r_home.content)

f = open('resultat.csv', 'w')
end = ''
if checkPrice:
    end = ';Modèle prix;prix'
f.write('Marque;Modèle;Version;Année;Mois;Ram;Identifiant;URL device;URL build' + end)
f.write('\n')
for item in listDevices:
    # Récupération du HTML
    constructor = item['name']
    for device in item['devices']:
        name_device = device['name']
        model = device['model']
        device_version_page = requests.get(apiUrl + 'devices/' + model, verify=False)
        device_version = json.loads(device_version_page.content)
        if len(device_version['versions']) > 0:
            version = device_version['versions'][len(device_version['versions'])-1]
        else:
            version = '?'
        device_wiki_page = requests.get(device_version['info_url'], verify=False)

        open('temp/' + model + '_device.txt', 'wb').write(device_wiki_page.content)
        # Récupération du nom
        file = open('temp/' + model + '_device.txt', "r", encoding='utf8')
        content_device = file.read()
        content_device = htmlmin.minify(content_device, remove_empty_space=True)
        file.close()
        # On vérifie que la page du device existe
        if(len(content_device.split('<h1>Page Not Found</h1>')) == 1 and len(content_device.split('Page Not Found | LineageOS Wiki')) == 1):
            #on regarde si des variantes existent
            if(len(content_device.split('/devices/' + model + '/variant1')) > 1):
                nbVariant = 1
                # on liste les variantes
                listeVariants = []
                while (len(content_device.split('/devices/' + model + '/variant' + str(nbVariant))) > 1):
                    listeVariants.append(device_version['info_url'] +'/variant' + str(nbVariant))
                    nbVariant += 1
                for variantUrl in listeVariants:
                    # pour chaque variant on récupère l'URL
                    device_wiki_page = requests.get(variantUrl, verify=False)
                    open('temp/' + model + '_device.txt', 'wb').write(device_wiki_page.content)
                    # Récupération du nom
                    file = open('temp/' + model + '_device.txt', "r", encoding='utf8')
                    content_device = file.read()
                    content_device = htmlmin.minify(content_device, remove_empty_space=True)
                    file.close()

                    # on reprend le programme normalement
                    tbody = content_device.split('<tbody>')
                    th = tbody[1].split('colspan=2')[1].split('<b>')
                    name_device = th[1].split('(' + model + ')')[0]
                    # Récupération de la date
                    if(len(content_device.split('Released</th><td>\n')) > 1):
                        treleased = content_device.split('Released</th><td>\n')
                    else:
                        treleased = content_device.split('Released</th><td>')
                    if not silent:
                        print(model)
                    date_device = treleased[1].split(' </td>')[0].strip()
                    if len(date_device.split(' ')) == 1:
                        oneModel = True
                        annee_device = date_device
                        mois_device = ''
                    elif len(date_device.split(' ')) == 2:
                        oneModel = True
                        annee_device = date_device.split(' ')[1]
                        mois_device = months[date_device.split(' ')[0]]
                    else:
                        if len(date_device.split('\n')) > 1:
                            oneModel = False
                            cpta = 0
                            annees = []
                            mois = []
                            modeles = []
                            if len(date_device.split(': ')) > 0:
                                for a in date_device.split('\n'):
                                    modeles.append(a.split(': ')[0])
                                    if(len(a.split(': ')[1].split(',')) > 1):
                                        annees.append(a.split(': ')[1].split(' ')[2].split('<br')[0])
                                    else:
                                        annees.append(a.split(': ')[1].split(' ')[1].split('<br')[0])
                                    mois.append(a.split(': ')[1].split(' ')[0])
                                    cpta = cpta + 1
                        else:
                            oneModel = True
                            annee_device = date_device.split(' ')[2]
                            mois_device = months[date_device.split(' ')[0]]
                    # Récupération de la ram
                    tram = content_device.split('RAM</th><td>')
                    ram_device = tram[1].split('</td>')[0]
                    # Ecriture des résultats
                    if oneModel:
                        end = ''
                        if checkPrice:
                            res = getPrice.getPrice(name_device,True)
                            end = ';' + res[0] + ';' + res[1]
                        f.write(constructor + ';' + name_device + ';' + version + ';' + annee_device + ';' + mois_device + ';' + ram_device + ';' + model + ';' + device_version['info_url'] + ';' + 'https://download.lineageos.org/devices/' + model + '/builds' + end)
                        f.write('\n')
                    else:
                        if cpta > 0:
                            i = 0
                            for m in modeles:
                                end = ''
                                if checkPrice:
                                    res = getPrice.getPrice(m,True)
                                    end = ';' + res[0] + ';' + res[1]
                                f.write(constructor + ';' + m + ';' + version + ';' + annees[i] + ';' + months[mois[i]] + ';' + ram_device + ';' + model + ';' + device_version['info_url'] + ';' + 'https://download.lineageos.org/devices/' + model + '/builds' + end)
                                f.write('\n')
                                i = i + 1
            else:
                tbody = content_device.split('<tbody>')
                th = tbody[1].split('colspan=2')[1].split('<b>')
                name_device = th[1].split('(' + model + ')')[0]
                # Récupération de la date
                if(len(content_device.split('Released</th><td>\n')) > 1):
                    treleased = content_device.split('Released</th><td>\n')
                else:
                    treleased = content_device.split('Released</th><td>')
                if not silent:
                    print(model)
                date_device = treleased[1].split(' </td>')[0].strip()
                if len(date_device.split(' ')) == 1:
                    oneModel = True
                    annee_device = date_device
                    mois_device = ''
                elif len(date_device.split(' ')) == 2:
                    oneModel = True
                    annee_device = date_device.split(' ')[1]
                    mois_device = months[date_device.split(' ')[0]]
                else:
                    if len(date_device.split('\n')) > 1:
                        oneModel = False
                        cpta = 0
                        annees = []
                        mois = []
                        modeles = []
                        if len(date_device.split(': ')) > 0:
                            for a in date_device.split('\n'):
                                modeles.append(a.split(': ')[0])
                                if(len(a.split(': ')[1].split(',')) > 1):
                                    annees.append(a.split(': ')[1].split(' ')[2].split('<br')[0])
                                else:
                                    annees.append(a.split(': ')[1].split(' ')[1].split('<br')[0])
                                mois.append(a.split(': ')[1].split(' ')[0])
                                cpta = cpta + 1
                    else:
                        oneModel = True
                        annee_device = date_device.split(' ')[2]
                        mois_device = months[date_device.split(' ')[0]]
                # Récupération de la ram
                tram = content_device.split('RAM</th><td>')
                ram_device = tram[1].split('</td>')[0]
                # Ecriture des résultats
                if oneModel:
                    end = ''
                    if checkPrice:
                        res = getPrice.getPrice(name_device,True)
                        end = ';' + res[0] + ';' + res[1]
                    f.write(constructor + ';' + name_device + ';' + version + ';' + annee_device + ';' + mois_device + ';' + ram_device + ';' + model + ';' + device_version['info_url'] + ';' + 'https://download.lineageos.org/devices/' + model + '/builds' + end)
                    f.write('\n')
                else:
                    if cpta > 0:
                        i = 0
                        for m in modeles:
                            end = ''
                            if checkPrice:
                                res = getPrice.getPrice(m,True)
                                end = ';' + res[0] + ';' + res[1]
                            f.write(constructor + ';' + m + ';' + version + ';' + annees[i] + ';' + months[mois[i]] + ';' + ram_device + ';' + model + ';' + device_version['info_url'] + ';' + 'https://download.lineageos.org/devices/' + model + '/builds' + end)
                            f.write('\n')
                            i = i + 1
f.close()

#generation du html
if not silent:
    file = open('html/modele.html', "r")
    modele_html = file.read()
    file.close()
    debut_html = modele_html.split('[CONTENT]')[0]
    fin_html = modele_html.split('[CONTENT]')[1]
    file = open('resultat.csv', 'r')
    resultats = file.read()
    file.close()
    f = open('html/index.html', 'w')
    f.write(debut_html)
    cpt = 0
    for device in resultats.split('\n'):
        if cpt == 0:
            f.write('<thead>\n')
            f.write('<tr style="background-color: #ebebeb;">\n')
            for champ in device.split(';'):
                f.write('<th class="header">' + champ.replace('è', '&egrave;') + '</th>\n')
        else:
            if cpt == 1:
                f.write('</thead>\n')
                f.write('<tbody>\n')
            f.write('<tr>\n')
            for champ in device.split(';'):
                f.write('<td class="td">' + champ + '</td>\n')
            f.write('</tr>\n')
        cpt = cpt + 1
    f.write('</tbody>\n')
    f.write(fin_html)
    f.close()
